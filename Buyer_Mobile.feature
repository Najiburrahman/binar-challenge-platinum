Feature: Buyer

    Background: User opens the app and view the Homepage

    Scenario: MBUY1-User views product based on category
        Given user view the Homepage
        When user check the product category section
        And user click on one of the Category button 
        Then page listed products based on the selected category 
    
    Scenario: MBUY2-User searches for a product
        Given user view the Homepage
        When user click on the Search field 
        And user fill in the Search field with product name
        And user select one of the product
        Then page listed products based on keyword 
        And user is redirected to the Product Description page 
    
    Scenario: MBUY3-User bargains a product without login
        Given user has not login into the app
        When user view the Homepage
        And user view the product list 
        And user select one of the product
        And user click on the Saya Tertarik dan Ingin Nego button
        Then user is redirected to the Login page

    Scenario: MBUY4-User bargains a product with valid data
        Given user has login into the app
        When user view the Homepage 
        And user view the product list
        And user select one of the product
        And user click on the Saya Tertarik dan Ingin Nego button
        And user fill in the Harga Tawar field
        And user click on the Kirim button
        Then confirmation message displayed informing your request has been submitted
    
    Scenario: MBUY5-User check transaction status of the bargain
        Given user has login into the app
        When user view the Homepage
        And user click on the menu Akun
        And user click on the menu Pesanan Saya
        Then the Pesanan Saya page displays the transaction status of the user