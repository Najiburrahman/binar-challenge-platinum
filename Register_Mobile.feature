Feature: Register Mobile

  Background: User opened the website and click Login button and click Register button
    Given user is in the Register page

    Scenario: MREG1-User Register using valid credentials
    When user input valid Name
    And user input valid Email
    And user input valid Password
    And user input valid Phone Number
    And user input valid City
    And user input valid Address
    And user click Daftar button
    Then user successfully Register
    And user get logged in using the registered account
    And user is directed to the Homepage
    
Scenario Outline: user can not register
    When user input valid Name
    And user input Email as "<condition>"
    And user input valid Password
    And user input valid Phone Number
    And user input valid City
    And user input valid Address
    And user click on Daftar button
    Then user can not Register
    And shows error message "<result>"

        Examples:
                  | case_id | condition      | result                  |
                  | MREG2   | blank values   | Please fill this field  |
                  | MREG3   | invalid format | Invalid Email           |
                  | MREG4   | Existing Email | Email already Exist     |

    Scenario: MREG5-User can not register with empty data
    When user leave Name field empty
    And user leave Email field empty
    And user leave Password field empty
    And user leave Phone Number field empty
    And user leave City field empty
    And user leave Address field empty
    And user click on Daftar button
    Then user can not Register
    And shows error message Please fill this field on Name field
