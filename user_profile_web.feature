Feature: user profile web


        Background: user is in user profile page
            Given user is in User Profile page

        Scenario: WPRO1-user add or edit user profile using valid values
             When user click on the Profile menu on the top right side
              And user click on the Your profile name link
              And user check the display of User Profile page
              And user check if the Name field displayed your registered name
              And user fill the Kota field with a valid option value
              And user fill the Alamat field with a valid value
              And user fill the No Handphone field with a valid value
              And user click on the Simpan button
             Then the user profile data is updated
              And a confirmation message appears

        Scenario: WPRO2-user can not add or can not edit user profile
             When user view the user Profile page
              And user does not fill in the Name field
              And user fill others field with valid format
              And user click on the Simpan button
             Then warning message is displayed in the Name field
              And user can't update their profile data

        Scenario: WPRO3-user can not add or can not edit user profile
             When user view the User Profile page
              And user does not fill in the City column
              And user fill others field with valid format
              And user click on the Simpan button
             Then warning message is displayed in the city field
              And user can't update their profile data

        Scenario: WPRO4-user can not add or can not edit user profile
             When user view the User Profile page
              And user does not fill in the Address field
              And user fill others field with valid format
              And user click on the Simpan button
             Then warning message is displayed in the address field
              And user can't update their profile data
        
        Scenario: WPRO5-user can not add or can not edit user profile
             When user view the User Profile page
              And the user does not fill in the Phone field
              And user fill others field with valid format
              And user click on the Simpan button
             Then warning message is displayed in the phone field
              And user can't update their profile data
        