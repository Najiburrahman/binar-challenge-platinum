Feature: Login

  Background: User opened the app, clicked on the Akun on the bottom right side, and then click on the Masuk button
    Given user is in the Login page

    Scenario: MLOG1-User login using valid credential
    When user input registered email address in Email field
    And user input registered password in Password field 
    And user click on Masuk button
    Then user can successfully login
    And user is directed to the Akun Saya page
    
    Scenario Outline: User can not login with invalid email 
        When user input "<condition_email>" in Email field
        And user input valid password in Password field
        And user click on Masuk button
        Then user can not log in
        And user see a warning message informing the inputted email address is "<condition_email>"
        Examples:
            | case_id | condition_email      | 
            | MLOG2   | empty field          |  
            | MLOG3   | invalid format       |         
            | MLOG4   | non registered email |       
    
    Scenario Outline: User can not login with invalid password
        When user input valid email in Email field
        And user input "<condition_password>" in Password field
        And user click on Masuk button
        Then user can not log in
        And user see warning message informing the inputted password is "<condition_password>"
        Example: 
            | case_id | condition_password | 
            | MLOG5   | invalid password   |
            | MLOG6   | empty field        |        
        



