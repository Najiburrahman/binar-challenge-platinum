Feature: user Profile mobile

        Background: user is in user profile page
            Given user is in User Profile page

        Scenario: MPRO1-user add or edit user profile using valid values
             When user click on the Akun menu on the bottom right side
              And user click on the Edit profile icon
              And user check the display of Info Akun page
              And user check each fields within Info Akun page
              And user click on the Simpan button
             Then user see each fields are displaying the correct registered data

        Scenario: MPRO2-user can not add or can not edit user profile
             When user view the User Profile page
              And user click on Nama field
              And user fill the Nama field with blank value
              And user click on Simpan button
             Then a warning message displayed in the Name field
              And the user can't update their profile data

        Scenario: MPRO3-user can not add or can not edit user profile
             When user view the User profile page
              And user click on Kota field
              And user fill the Kota field with blank value
              And user click on Simpan button
             Then a warning message displayed in the Kota field
              And the user can't update their profile data

        Scenario: MPRO4-user can not add or can not edit user profile
             When user view the User profile page
              And user click on Nomor HP field
              And user fill the Nomor HP field with blank value
              And user click on Simpan button
             Then a warning message displayed in the Nomor Hp field
              And the user can't update their profile data

        Scenario: MPRO5-user can not add or can not edit user profile
             When user view the User profile page
              And user click on Alamat field
              And user fill the Alamat field with blank value
              And user click on Simpan button
             Then a warning message displayed in the Alamat field
              And the user can't update their profile data

        

