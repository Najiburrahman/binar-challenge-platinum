Feature: Login

  Background: User opened the website and view the Login page by clicking the Masuk button on the top right side
    Given user is in the Login page

    Scenario: WLOG1-User login using valid credential
    When user input registered email address in Email field
    And user input registered password in Password field 
    And user click on Masuk button
    Then user can successfully login
    And user is directed to the login version of Homepage
    
    Scenario Outline: User can not login with invalid email address
        When user input "<condition_email>" in Email field
        And user input valid password in Password field
        And user click on Masuk button
        Then user can not log in
        And user see a warning message informing the inputted email address is "<condition_email>"
        Examples:
            | case_id | condition_email      | 
            | WLOG2   | empty field          | 
            | WLOG3   | invalid format       |         
            | WLOG4   | non registered email |       

   
    Scenario Outline: User can not login with invalid password
        When user input valid email in Email field
        And user input "<condition_password>" password in Password field
        And user click on Masuk button
        Then user can not log in
        And user see warning message informing the inputted password is "<condition_password>"
        Example: 
            | case_id | condition_password | 
            | WLOG5   | invalid password   |
            | WLOG6   | empty field        |    
