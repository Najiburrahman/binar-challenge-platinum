Feature: Seller

    Background: User opens the apps and view the Homepage

     Scenario: MADD1-User opens the Add Product Page from Homepage
     Given user is logged into the apps
     When user click on the Jual button
     And user view the display of Add Product page
     Then user can see all neccessary fields
     
     Scenario: MADD2-User opens the Add Product Page without logging in
     Given user has not logged into the apps
     When user click on the Jual button
     Then user is redirected to the login page

     Scenario: MADD3-User adds new product by filling empty fields in Description field
     Given user opened the Add Product Page and has logged in
     When user view the Add Product page
     And user leave the Description fields fields empty
     And user fill the Nama Product fields with valid data
     And user fill the Harga Product fields with valid data
     And user fill the Category fields with valid data
     And user fill the Image fields with valid data
     And user click on the Terbitkan button
     Then warning message displayed
     And user cannot add a new product

     Scenario: MADD4-User adds new product by filling empty fields in Nama Product field
     Given user opened the Add Product Page and has logged in
     When user view the Add Product page
     And user leave the Nama Product fields empty
     And user fill the Harga Product fields with valid data
     And user fill the Category fields with valid data
     And user fill the Description fields with valid data
     And user fill the Image fields with valid data
     And user click on the Terbitkan button
     Then warning message displayed
     And user cannot add a new product

     Scenario: MADD5-User adds new product by filling empty fields in Harga Product field
     Given user opened the Add Product Page and has logged in
     When user view the Add Product page
     And user fill the Harga Product fields empty
     And user fill the Nama Product fields with valid data     
     And user fill the Category fields with valid data
     And user fill the Description fields with valid data
     And user fill the Image fields with valid data
     And user click on the Terbitkan button
     Then warning message displayed
     And user cannot add a new product