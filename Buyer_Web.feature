Feature: Buyer

    Background: User opens the website and view the Homepage

    Scenario: WBUY1-User views product based on category
        Given user view the Homepage
        When user check the product category section
        And user click on one of the Category button 
        Then page listed products based on the selected category

    Scenario: WBUY2-User searches for a product
        Given user view the Homepage
        When user click on the Search field
        And user fill in the Search field with product name
        And user press Enter
        Then product list section displays products based on keyword 
  
    Scenario: WBUY3-User bargains a product without login
        Given user has not login into the website
        When user view the Homepage
        And user view the product list
        And user select one of the product
        And user click on the Saya Tertarik dan Ingin Nego button
        And user fill in the Harga Tawar field
        And user click on the Kirim button
        Then user is redirected to the Login page

    Scenario: WBUY4-User bargains a product without completing the user profile
        Given user has login but not yet completed the user profile
        When user view the Homepage
        And user view the product list
        And user select one of the product
        And user click on the Saya Tertarik dan Ingin Nego button
        And user fill in the Harga Tawar field
        And user click on the Kirim button
        Then user is redirected to the Lengkapi Info Akun page

    Scenario: WBUY5-User bargains a product with valid data
        Given user has login into the website
        When user view the Homepage
        And user view the product list
        And user select one of the product
        And user click on the Saya Tertarik dan Ingin Nego button
        And user fill in the Harga Tawar field
        And user click on the Kirim button
        Then confirmation message displayed informing your request has been submitted